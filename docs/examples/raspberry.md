# Bootstrap your own Raspberry OS image with prtg-pyprobe
## Using [pi-gen](https://github.com/RPi-Distro/pi-gen)
- Clone the [prtg pyprobe repo](https://gitlab.com/paessler-labs/prtg-pyprobe)
- Copy the contents of deploment/pigen:  
`cp -r deployment/pi-gen/04-pyprobe pi-gen/stage2`  
`cp deployment/pi-gen/config pi-gen/`
- As we want a headless system, skip later build stages:  
`touch pi-gen/stage3/SKIP pi-gen/stage4/SKIP pi-gen/stage5/SKIP`  
`touch pi-gen/stage4/SKIP_IMAGES pi-gen/stage5/SKIP_IMAGES`
- Now to make sure you don't have to have all the needed dependencies installed, we will use docker to bootstrap the image:  
`sudo ./build-docker.sh`
- After a (long) while your image will show up under `pi-gen/work` and you can flash it to an SD card.

 
