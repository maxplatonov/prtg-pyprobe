---
**WARNING**  
Although this sensor can be added to every device on the probe, it will **ONLY** monitor the probe itself.  

---
#### Description
Returns the external ip address of the probe.

---
#### Configuration
**Timeout**:  If the reply takes longer than this value the request is aborted and an error message is triggered. 

---
![External IP](../img/sensor_ext_ip.png){: align=left }
