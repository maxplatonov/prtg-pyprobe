# pyprobe for PRTG
A platform independent implementation of a PRTG probe which communicates via the [miniprobe api](https://www.paessler.com/manuals/prtg/mini_probe_api).

---
#### Important
This is **not** a full PRTG remote probe and therefore has only a [limited set](https://www.paessler.com/manuals/prtg/mini_probe_api#probe_types) of functions and sensors. These sensors are also **not** the same sensors that are built into the normal PRTG remote probe, they function differently and offer different capabilities.  

---
## Information
### Documentation
The documentation is available [here](https://paessler-labs.gitlab.io/prtg-pyprobe/).

### Issue tracker
If you should encounter any bugs, issues or have feedback please head over to the [issue tracker](https://gitlab.com/paessler-labs/prtg-pyprobe/-/issues).

## Contributors
Konstantin Wolff, Paessler AG ([@devKonz](https://twitter.com/devKonz))  
Greg Campion, Paessler AG ([@cainde](https://twitter.com/cainde9))  
Torsten Lindner, Paessler AG  
Max Funke, Paessler AG ([@maxfunke](https://twitter.com/maxfunke))  
Erhard Mikulik, Paessler AG ([@mikonaut](https://twitter.com/mikonaut))  
