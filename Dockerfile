FROM python:3.8.5-slim-buster as build

RUN apt-get update && apt-get install -y gcc libyaml-0-2 libyaml-dev g++

# set up a workdir to reduce clutter
WORKDIR build-pyprobe
# Sets utf-8 encoding for Python
ENV LANG=C.UTF-8
# Turns off writing .pyc files; superfluous on an ephemeral container.
ENV PYTHONDONTWRITEBYTECODE=1
# Seems to speed things up
ENV PYTHONUNBUFFERED=1

# Ensures that the python and pip executables used
# in the image will be those from our virtualenv.
ENV PATH="/pyprobe-venv/bin:$PATH"

# Setup the virtualenv
RUN python -m venv /pyprobe-venv

# gather needed files
COPY ./LICENSE.txt /build-pyprobe/LICENSE.txt
COPY ./setup.py /build-pyprobe/setup.py
COPY ./setup.cfg /build-pyprobe/setup.cfg
COPY ./requirements.txt /build-pyprobe/requirements.txt
COPY prtg_pyprobe /build-pyprobe/prtg_pyprobe

# prepare install
RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir wheel
RUN pip install --no-cache-dir -r requirements.txt
# install
RUN python -m pip install --no-cache-dir . -vvv 

## create a small release image 
FROM python:3.8.5-slim-buster as release

ENV PATH="/pyprobe-venv/bin:$PATH"

COPY --from=build /pyprobe-venv /pyprobe-venv
WORKDIR /pyprobe-venv/lib/python3.8/site-packages/prtg_pyprobe
ENTRYPOINT ["python", "/pyprobe-venv/lib/python3.8/site-packages/prtg_pyprobe/run.py"]
