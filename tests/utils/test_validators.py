from prtg_pyprobe.utils.validators import (
    validation,
    ValidationFailedError,
    ValidationNotPossibleError,
    validate,
    validation_schemes,
    unit_kind_allowed,
    field_type_allowed,
)


@validate
def validation_not_possible(test=None):
    x = test
    return x


class TestValidation:
    def test_validation_success(self):
        test_scheme = {"test": {"type": "string"}}
        data = {"test": "works"}
        assert validation(val=data, scheme=test_scheme) is data

    def test_validation_failed(self):
        test_scheme = {"test": {"type": "string"}}
        data = {"test": 1}
        try:
            validation(val=data, scheme=test_scheme)
        except ValidationFailedError as e:
            assert str(e) == "{'test': ['must be of string type']}"

    def test_validation_not_possible(self):
        try:
            validation_not_possible("Validation not possible")
        except ValidationNotPossibleError as e:
            assert str(e) == "Cannot validate, no validation schema defined."

    def test_validation_scheme_sensor_data_channel(self):
        assert type(validation_schemes["SensorDataChannel"]) is dict
        assert validation_schemes["SensorDataChannel"] == {
            "name": {"type": "string", "required": True, "empty": False},
            "mode": {
                "type": "string",
                "required": True,
                "allowed": ["integer", "float", "counter"],
                "empty": False,
            },
            "value": {"type": ["integer", "float"]},
            "unit": {"type": "string", "allowed": unit_kind_allowed},
            "kind": {"type": "string", "allowed": unit_kind_allowed},
            "customunit": {"type": "string"},
        }

    def test_validation_scheme_sensor_data(self):
        assert type(validation_schemes["SensorData"]) is dict
        assert validation_schemes["SensorData"] == {
            "sensorid": {"type": "string", "required": True, "empty": False},
            "message": {"type": "string"},
            "channel": {"type": "list"},
            "error": {"type": "string", "dependencies": ["message", "code"]},
            "code": {"type": "integer"},
        }

    def test_validation_scheme_sensor_definition(self):
        assert type(validation_schemes["SensorDefinition"]) is dict
        assert validation_schemes["SensorDefinition"] == {
            "name": {"type": "string", "required": True, "empty": False},
            "kind": {"type": "string", "required": True, "empty": False},
            "description": {"type": "string", "required": True, "empty": False},
            "help": {"type": "string", "required": True, "empty": False},
            "tag": {"type": "string", "required": True, "empty": False},
            "default": {"type": "string", "allowed": ["yes", "1", "true"]},
            "groups": {"type": "list", "required": True, "empty": True},
        }

    def test_validation_scheme_sensor_definition_group(self):
        assert type(validation_schemes["SensorDefinitionGroup"]) is dict
        assert validation_schemes["SensorDefinitionGroup"] == {
            "caption": {"empty": False, "required": True, "type": "string"},
            "fields": {"empty": True, "required": False, "type": "list"},
            "name": {"empty": False, "regex": "^[a-z0-9_-]{5,40}", "required": True, "type": "string"},
        }

    def test_validation_scheme_sensor_definition_group_field(self):
        assert type(validation_schemes["SensorDefinitionGroupField"]) is dict
        assert validation_schemes["SensorDefinitionGroupField"] == {
            "name": {"type": "string", "required": True, "empty": False},
            "type": {
                "type": "string",
                "required": True,
                "allowed": field_type_allowed,
                "empty": False,
            },
            "caption": {"type": "string", "required": True, "empty": False},
            "required": {"type": "string", "allowed": ["yes", "1", "true"]},
            "help": {"type": "string"},
            "default": {"type": ["string", "integer"]},
            "minimum": {"type": "integer"},
            "maximum": {"type": "integer"},
            "options": {
                "type": "dict",
                "keysrules": {"type": "string"},
                "valuesrules": {"type": "string"},
            },
        }
