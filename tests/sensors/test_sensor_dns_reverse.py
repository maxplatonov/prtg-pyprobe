import asyncio

import dns.reversename as dns_rev_name
import pytest
from dns.asyncresolver import Resolver
from dns.exception import DNSException

from prtg_pyprobe.sensors import sensor_dns_reverse as module
from prtg_pyprobe.sensors.sensor import SensorBase
from tests.conftest import dns_answer


def dns_answer_dns_exception(*args, **kwargs):
    raise DNSException


def address_from_ip(*args, **kwargs):
    return "123"


def task_data():
    return {
        "sensorid": "1234",
        "host": "8.8.8.8",
        "ip": "123.456.789.0",
        "port": "53",
        "timeout": "5",
        "kind": "mpdns",
    }


class TestDNSReverseSensorProperties:
    def test_sensor_dns_reverse_base_class(self, dns_reverse_sensor):
        assert type(dns_reverse_sensor).__bases__[0] is SensorBase

    def test_sensor_dns_reverse_name(self, dns_reverse_sensor):
        assert dns_reverse_sensor.name == "DNS Reverse"

    def test_sensor_dns_reverse_kind(self, dns_reverse_sensor):
        assert dns_reverse_sensor.kind == "mpdnsptr"

    def test_sensor_dns_reverse_definition(self, dns_reverse_sensor):
        assert dns_reverse_sensor.definition == {
            "description": "DNS PTR Lookup",
            "groups": [
                {
                    "caption": "DNS Specific",
                    "fields": [
                        {
                            "caption": "Timeout (in s)",
                            "default": 5,
                            "help": "If the reply takes longer than this value "
                            "the request is aborted and an error message "
                            "is triggered. Maximum value is 900 sec. (= "
                            "15.0 minutes)",
                            "maximum": 900,
                            "minimum": 1,
                            "name": "timeout",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "Port",
                            "default": 53,
                            "help": "Enter the port on which the DNS service of " "the parent device is running.",
                            "maximum": 65535,
                            "minimum": 1,
                            "name": "port",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "IP Address",
                            "help": "Enter am IP Address to resolve.",
                            "name": "ip",
                            "required": "1",
                            "type": "edit",
                        },
                    ],
                    "name": "dns_specific",
                }
            ],
            "help": "The DNS sensor monitors a Domain Name Service (DNS) server. It "
            "resolves a domain name and compares it to a given IP address.",
            "kind": "mpdnsptr",
            "name": "DNS Reverse",
            "tag": "mpdnssensor",
        }


@pytest.mark.asyncio
class TestDNSSensorWork:
    async def test_sensor_dns_reverse_fail(self, dns_reverse_sensor, monkeypatch, sensor_exception_message, mocker):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Resolver, "resolve", dns_answer_dns_exception)
        dns_q = asyncio.Queue()
        await dns_reverse_sensor.work(task_data=task_data(), q=dns_q)
        out = await dns_q.get()
        sensor_exception_message["message"] = "DNS check failed. See log for details"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_with("Error in dnspython.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_dns_reverse_work_success(self, dns_reverse_sensor, monkeypatch):
        monkeypatch.setattr(Resolver, "resolve", dns_answer)
        monkeypatch.setattr(dns_rev_name, "from_address", address_from_ip)
        dns_q = asyncio.Queue()
        await dns_reverse_sensor.work(task_data=task_data(), q=dns_q)
        out = await dns_q.get()
        assert out["sensorid"] == "1234"
        assert out["message"] == "Query Type: PTR for 123.456.789.0. Result: mytarget, "
        assert len(out["channel"]) == 1
