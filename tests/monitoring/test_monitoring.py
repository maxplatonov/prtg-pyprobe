import asyncio

import pytest
from httpx import Response
from pysnmp.hlapi.asyncio import SnmpEngine

from prtg_pyprobe.communication.communication_http_api import PRTGHTTPApi
from prtg_pyprobe.monitoring.monitoring import (
    run_monitoring_tasks,
    monitoring,
)
from prtg_pyprobe.sensors.sensor_snmp_custom import Sensor as SnmpCustom
from tests.conftest import asyncio_open_connection


async def response_200(*args, **kwargs):
    return Response(status_code=200)


@pytest.mark.asyncio
class TestMonitoring:
    async def test_monitoring_run_monitoring_tasks_no_snmp(
        self, ping_sensor, monkeypatch, prtg_api, task_data_ping, probe_config_dict
    ):
        monkeypatch.setattr(asyncio, "wait_for", asyncio_open_connection)
        monkeypatch.setattr(PRTGHTTPApi, "send_data", response_200)
        snmp_engine = SnmpEngine()
        test_queue = asyncio.Queue()
        test_sensor_list = [ping_sensor]
        res = await run_monitoring_tasks(
            sensors_avail=test_sensor_list,
            taskdata=task_data_ping,
            snmp_engine=snmp_engine,
            async_queue=test_queue,
            prtg_api=prtg_api,
            config=probe_config_dict,
        )
        assert not res

    async def test_monitoring_run_monitoring_tasks_snmp(self, monkeypatch, prtg_api, task_data_snmp, probe_config_dict):
        monkeypatch.setattr(PRTGHTTPApi, "send_data", response_200)
        snmp_engine = SnmpEngine()
        test_queue = asyncio.Queue()
        test_snmp_sensor_list = [SnmpCustom()]
        res = await run_monitoring_tasks(
            sensors_avail=test_snmp_sensor_list,
            taskdata=task_data_snmp,
            snmp_engine=snmp_engine,
            async_queue=test_queue,
            prtg_api=prtg_api,
            config=probe_config_dict,
        )
        assert not res

    async def test_monitoring_monitoring(self, ping_sensor, prtg_api, monkeypatch, task_data_ping, probe_config_dict):
        monkeypatch.setattr(PRTGHTTPApi, "send_data", response_200)
        snmp_engine = SnmpEngine()
        test_sensor_list = [ping_sensor]
        res = await monitoring(
            tasks_list=task_data_ping,
            sensor_objects=test_sensor_list,
            snmp_engine=snmp_engine,
            prtg_api=prtg_api,
            config=probe_config_dict,
        )
        assert not res
