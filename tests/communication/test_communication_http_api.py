import pytest
from requests.exceptions import RetryError, ConnectionError

from prtg_pyprobe.communication import communication_http_api as module
from prtg_pyprobe.communication.communication_http_api import PRTGHTTPApi


class TestProbeConnection:
    def test__send_api_request(self, mocker, probe_config_dict):
        requests_mock = mocker.patch.object(module, "requests")
        requests_mock.Session.return_value.post.return_value.status_code = 200
        result = PRTGHTTPApi(probe_config_dict, 0.1)._send_api_request()

        assert result.status_code == 200
        requests_mock.Session.return_value.post.assert_called_with(data=None, params=None, url=None, verify=False)

    def test__send_api_request_401(self, mocker, probe_config_dict):
        requests_mock = mocker.patch.object(module, "requests")
        requests_mock.Session.return_value.post.return_value.status_code = 401
        logging_mock = mocker.patch.object(module, "logging")
        result = PRTGHTTPApi(probe_config_dict, 0.1)._send_api_request()

        assert result.status_code == 401
        requests_mock.Session.return_value.post.assert_called_with(data=None, params=None, url=None, verify=False)
        logging_mock.error.assert_called_with("Cannot connect to the PRTG server, have you approved your probe?")

    def test__send_api_request_verify(self, mocker, probe_config_dict):
        requests_mock = mocker.patch.object(module, "requests")
        requests_mock.Session.return_value.post.return_value.status_code = 200
        probe_config_dict["disable_ssl_verification"] = False
        result = PRTGHTTPApi(probe_config_dict, 0.1)._send_api_request()

        assert result.status_code == 200
        requests_mock.Session.return_value.post.assert_called_with(data=None, params=None, url=None, verify=True)

    def test_backoff(self, mocker, probe_config_dict):
        logging_mock = mocker.patch.object(module, "logging")
        probe_config_dict["disable_ssl_verification"] = False

        http_api_class = PRTGHTTPApi(probe_config_dict, 0.01)

        with pytest.raises(RetryError):
            http_api_class._send_api_request(api_url="https://httpbin.org/status/500")

        logging_mock.exception.assert_called_with("Max connection retries exceeded, backing off!")

    def test_connection_failed(self, mocker, probe_config_dict):
        logging_mock = mocker.patch.object(module, "logging")

        http_api_class = PRTGHTTPApi(probe_config_dict, 0.02)

        with pytest.raises(ConnectionError):
            http_api_class._send_api_request(api_url="https://kljnasdlkjasdlkajsdlkj.org")

        logging_mock.exception.assert_called_with("Cannot connect to server, please check your config")

    def test_send_announce(self, mocker, probe_config_dict, list_sensor_objects):
        send_api_request_mock = mocker.patch.object(PRTGHTTPApi, "_send_api_request")
        logging_mock = mocker.patch.object(module, "logging")

        PRTGHTTPApi(probe_config_dict, 0.1).send_announce(
            sensor_definitions=[{"Im a little": "teapot short and stout"}]
        )

        send_api_request_mock.assert_called_with(
            api_url="https://test.prtg.com:443/probe/announce",
            post_data={
                "key": "cd7b773e2ce4205e9f5907b157f3d26495c5b373",
                "protocol": 1,
                "gid": "1BFB9273-08D7-43AF-B535-18E4A767BA34",
                "probe_base_interval": "60",
                "sensors": '[{"Im a little": "teapot short and stout"}]',
                "name": "Python Mini Probe",
            },
        )
        logging_mock.info.assert_called_with("Sending Announce request to PRTG Core at https://test.prtg.com:443")
        logging_mock.debug.assert_called_once()

    @pytest.mark.asyncio
    async def test_send_task(self, mocker, probe_config_dict):
        send_api_request_mock = mocker.patch.object(PRTGHTTPApi, "_send_api_request_async")
        logging_mock = mocker.patch.object(module, "logging")

        await PRTGHTTPApi(probe_config_dict, 0.1).get_tasks()

        send_api_request_mock.assert_called_with(
            api_url="https://test.prtg.com:443/probe/tasks",
            post_data={
                "key": "cd7b773e2ce4205e9f5907b157f3d26495c5b373",
                "protocol": 1,
                "gid": "1BFB9273-08D7-43AF-B535-18E4A767BA34",
            },
        )
        logging_mock.info.assert_called_with("Sending Task request to PRTG Core at https://test.prtg.com:443")
        logging_mock.debug.assert_called_once()

    @pytest.mark.asyncio
    async def test_send_data(self, mocker, probe_config_dict, sensor_data):
        send_api_request_mock = mocker.patch.object(PRTGHTTPApi, "_send_api_request_async")
        logging_mock = mocker.patch.object(module, "logging")

        await PRTGHTTPApi(probe_config_dict, 0.1).send_data([{"Im a little": "teapot short and stout"}])

        send_api_request_mock.assert_called_with(
            api_url="https://test.prtg.com:443/probe/data",
            post_data='[{"Im a little": "teapot short and stout"}]',
            url_params={
                "gid": "1BFB9273-08D7-43AF-B535-18E4A767BA34",
                "key": "cd7b773e2ce4205e9f5907b157f3d26495c5b373",
                "protocol": 1,
            },
        )
        logging_mock.info.assert_called_with("Sending Data Request to PRTG Core at https://test.prtg.com:443")
        logging_mock.debug.assert_called_once()
